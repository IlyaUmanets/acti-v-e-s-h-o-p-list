class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    can [:new, :create], User
    can [:show, :edit, :update], User, id: user.id
    if user.persisted?
      can [:show, :index], Shop
      can [:show, :index], Category
      can [:manage], Social, user_id: user.id
      can [:new, :create], List
      can [:show, :index], List, id: user.list_ids
      can :manage, List, owner_id: user.id
      can [:new, :create, :edit, :update, :show, :index], ListItem, list_id: user.list_ids
      can [:destroy], ListItem, list_id: user.lists.map { |list| list.owner_id == user.id }
    end
    if user.is_admin
      can :manage, :all
    end
  end
end

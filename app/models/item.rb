# == Schema Information
#
# Table name: items
#
#  id          :integer          not null, primary key
#  name        :string
#  category_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  description :string
#

class Item < ActiveRecord::Base
  belongs_to :category
  has_many :list_items

  validates_presence_of :name, :category_id
end

# == Schema Information
#
# Table name: lists
#
#  id         :integer          not null, primary key
#  shop_id    :integer
#  name       :string
#  alarm      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  group_id   :integer
#  owner_id   :integer
#

class List < ActiveRecord::Base
  belongs_to :shop
  belongs_to :group
  has_many :list_items
  has_and_belongs_to_many :users

  # after_save :destroy_unused if proc { |list| list.users.count.zero? }

  validates_presence_of :name
  private

  # destroy list if it has no users
  def destroy_unused
    self.destroy!
  end
end

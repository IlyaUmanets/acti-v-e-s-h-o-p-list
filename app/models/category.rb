# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  parent_id  :integer
#

class Category < ActiveRecord::Base
  has_many :items
  has_many :sub_categories, class_name: 'Category', foreign_key: 'parent_id'

  validates_presence_of :name
end

# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  auth_token :string
#  is_admin   :boolean          default(FALSE)
#

class User < ActiveRecord::Base
  # TODO: validate socials uniq by name
  has_many :socials, dependent: :destroy
  has_many :groups, foreign_key: 'owner_id'

  has_many :friendships
  has_many :friends, through: :friendships

  has_many :inverse_friendships, class_name: 'Friendship', foreign_key: 'friend_id'
  has_many :inverse_friends, through: :inverse_friendships, source: :user

  has_and_belongs_to_many :lists
  has_and_belongs_to_many :shops
  has_and_belongs_to_many :groups

  validates :name, presence: true
  validates :auth_token, presence: true
end

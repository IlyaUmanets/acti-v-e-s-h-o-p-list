# == Schema Information
#
# Table name: socials
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  uid        :integer
#  provider   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Social < ActiveRecord::Base
  belongs_to :user
  # TODO: list of available providers
  validates_presence_of :uid, :provider
end

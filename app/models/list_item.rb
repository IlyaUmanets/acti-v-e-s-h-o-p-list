# == Schema Information
#
# Table name: list_items
#
#  id         :integer          not null, primary key
#  item_id    :integer
#  count      :float
#  is_bought  :boolean
#  is_cancel  :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  list_id    :integer
#

class ListItem < ActiveRecord::Base
  belongs_to :item
  belongs_to :list

  validates_presence_of :item_id, :count, :list_id

end

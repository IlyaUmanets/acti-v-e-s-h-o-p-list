# == Schema Information
#
# Table name: groups
#
#  id         :integer          not null, primary key
#  owner_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  name       :string
#

class Group < ActiveRecord::Base
  belongs_to :owner, class_name: 'User', foreign_key: 'owner_id'

  has_one :list
  has_and_belongs_to_many :users

  validates_presence_of :owner_id, :name
end

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  load_and_authorize_resource

  protect_from_forgery with: :null_session

  before_action :create_session

  # TODO: update for every user session
  def create_session
    @auth_token = ApiKey.create!.access_token
  end

  helper_method :current_user

  def current_ability
    if current_user
      @current_ability ||= Ability.new(current_user)
    else
      @current_ability ||= Ability.new(User.new)
    end
  end

  def current_user
    @current_user = User.find_by_auth_token(params[:auth_token])
  end
end

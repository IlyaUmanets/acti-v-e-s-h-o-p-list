class Api::V1::CategoriesController < ApplicationController

  swagger_controller :categories, 'Category Management'

  swagger_model :Category do
    description 'A Category object.'
    property :id, :integer, :required, 'Category Id'
    property :name, :string, :required, 'Name'
    property :parent_id, :integer, :required, 'Parent Id'
    property :created_at, :string, :optional, 'Created at'
    property :updated_at, :string, :optional, 'Update at'
  end

  swagger_api :index do
    summary 'Fetches all Category items'
    notes 'Dont work, try in Postman'
  end

  swagger_api :show do
    summary 'Fetches a single Category item'
    param :path, :id, :integer, :optional, 'Category Id'
    response :ok, 'Success', :Category
    response :not_found
  end

  swagger_api :create do
    summary 'Creates a new Category'
    param :form, 'category[name]', :string, :required, 'Category name'
    response :created, 'created', :Category
    response :not_acceptable
  end

  swagger_api :update do
    summary 'Updates an existing category'

    param :path, :id, :integer, :required, 'Category Id'
    param :form, 'category[name]', :string, :required, 'Name'
    response :ok, 'ok', :Category
    response :not_found
  end

  # dont show 200
  swagger_api :destroy do
    summary 'Deletes an existing category item'
    notes 'Status 200 dont work, try in Postman'
    param :path, :id, :integer, :required, 'Category Id'
    response :ok, 'ok', :Category
    response :not_found
  end

  def index
    @categories = Category.all
    render json: @categories, status: :ok, include: [:items, :sub_categories]
  end

  def show
    @category = Category.find(params[:id])
    if @category
      render json: @category, status: :ok, include: [:items, :sub_categories]
    else
      head 404
    end
  end

  def new
    @category = Category.new
    render json: @category, status: :ok, include: [:items, :sub_categories]
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      render json: @category, status: :created, include: [:items, :sub_categories]
    else
      render json: @category.errors, status: 422
    end
  end

  def edit
    @category = Category.find(params[:id])
    if @category
      render json: @category, status: :ok, include: [:items, :sub_categories]
    else
      head 404
    end
  end

  def update
    @category = Category.find(params[:id])
    head 404 unless @category
    @category = Category.update(category_params)
    if @category.save
      render json: @category, status: :ok, include: [:items, :sub_categories]
    else
      render json: @category.errors, status: :ok
    end
  end

  def destroy
    @category = Category.find(params[:id])
    head 404 unless @category
    @category.destroy
    head :ok
  end

  private

  def category_params
    params.require(:category).permit(:name)
  end
end

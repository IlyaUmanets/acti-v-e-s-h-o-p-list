class Api::V1::SocialsController < ApplicationController
  # FIXME: enable later
  # before_filter :restrict_access

  swagger_controller :socials, 'Shop Management'

  swagger_model :Social do
    description 'A Social object'
    property :id, :integer, :required, 'Social Id'
    property :user_id, :integer, :required, 'User Id'
    property :uid, :integer, :required, 'uid'
    property :provider, :string, :required, 'provider'
    property :created_at, :string, :optional, 'Created at'
    property :updated_at, :string, :optional, 'Update at'
  end

  swagger_api :index do
    summary 'Fetches all Social objects'
    notes 'Dont work, try in Postman'
  end

  swagger_api :show do
    summary 'Fetches a single Social object'
    param :path, :id, :integer, :optional, 'Shop Id'
    response :ok, 'Success', :Shop
    response :not_found
  end

  swagger_api :create do
    summary 'Creates a new Social'
    param :form, 'social[uid]', :integer, :required, 'uid'
    param :form, 'social[provider]', :string, :required, 'provider'
    response :created, 'created', :Social
    response :not_acceptable
  end

  swagger_api :update do
    summary 'Updates an existing Social'

    param :path, :id, :integer, :required, 'Social Id'
    param :form, 'social[uid]', :integer, :required, 'uid'
    param :form, 'social[provider]', :string, :required, 'provider'
    response :ok, 'ok', :Shop
    response :not_found
  end

  swagger_api :destroy do
    summary 'Deletes an existing Social object'
    notes 'Status 200 dont work, try in Postman'
    param :path, :id, :integer, :required, 'Social Id'
    response :ok, 'ok', :Shop
    response :not_found
  end

  def index
    @socials = Social.all
    render json: @socials, status: :ok, include: [:user]
  end

  def show
    @social = Social.find(params[:id])
    if @social
      render json: @social, status: :ok, include: [:user]
    else
      head 404
    end
  end

  def new
    @social = Social.new
    render json: @social, status: :ok, include: [:user]
  end

  def create
    @social = Social.new(social_params)
    if @social.save
      render json: @social, status: :created, include: [:user]
    else
      render json: @social.errors, status: 422
    end
  end

  def edit
    @social = Social.find(params[:id])
    if @social
      render json: @social, status: :ok, include: [:user]
    else
      head 404
    end
  end

  def update
    @social = Social.find(params[:id])
    head 404 unless @social
    @social = Social.update(social_params)
    if @social.save
      render json: @social, status: :ok, include: [:user]
    else
      render json: @social.errors, status: :ok
    end
  end

  def destroy
    @social = Social.find(params[:id])
    head 404 unless @social
    @social.destroy
    head :ok
  end

  def add_social
    @social = Social.new(social_params)
    if @social.save
      User.find(params[:user_id]).socials << @social
    end
  end

  private

  def social_params
    params.require(:social).permit(:uid, :provider)
  end

  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end
end

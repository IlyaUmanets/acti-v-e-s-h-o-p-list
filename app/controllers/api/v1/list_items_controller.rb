class Api::V1::ListItemsController < ApplicationController
  swagger_controller :list_items, 'ListItem Management'

  swagger_model :ListItem do
    description 'A ListItem object.'
    property :id, :integer, :required, 'ListItem Id'
    property :item_id, :integer, :required, 'Item Id'
    property :list_id, :integer, :required, 'List Id'
    property :count, :float, :required, 'count'
    property :is_bought, :boolean, :required, 'is bought'
    property :is_cancel, :boolean, :required, 'is_cancel'
    property :created_at, :string, :optional, 'Created at'
    property :updated_at, :string, :optional, 'Update at'
  end

  swagger_api :index do
    summary 'Fetches all ListItem objects'
    notes 'Dont work, try in Postman'
  end

  swagger_api :show do
    summary 'Fetches a single ListItem object'
    param :path, :id, :integer, :optional, 'ListItem Id'
    response :ok, 'Success', :ListItem
    response :not_found
  end

  swagger_api :create do
    summary 'Creates a new ListItem'
    param :form, 'list_item[item_id]', :integer, :required, 'Item id'
    param :form, 'list_item[list_id]', :integer, :required, 'List id'
    param :form, 'list_item[count]', :integer, :required, 'count'
    param :form, 'list_item[is_bought]', :boolean, :required, 'is_bought'
    param :form, 'list_item[is_cancel]', :boolean, :required, 'is_cancel'
    response :created, 'created', :ListItem
    response :not_acceptable
  end

  swagger_api :update do
    summary 'Updates an existing ListItem'

    param :path, :id, :integer, :required, 'ListItem Id'
    param :form, 'list_item[item_id]', :integer, :required, 'Item id'
    param :form, 'list_item[list_id]', :integer, :required, 'List id'
    param :form, 'list_item[count]', :integer, :required, 'count'
    param :form, 'list_item[is_bought]', :boolean, :required, 'is_bought'
    param :form, 'list_item[is_cancel]', :boolean, :required, 'is_cancel'
    response :ok, 'ok', :Item
    response :not_found
  end

  swagger_api :destroy do
    summary 'Deletes an existing ListItem object'
    notes 'Status 200 dont work, try in Postman'
    param :path, :id, :integer, :required, 'ListItem Id'
    response :ok, 'ok', :Item
    response :not_found
  end

  def index
    @list_items = ListItem.all
    render json: @list_items, status: :ok, include: [:item, :list]
  end

  def show
    @list_item = ListItem.find(params[:id])
    if @list_item
      render json: @list_item, status: :ok, include: [:item, :list]
    else
      head 404
    end
  end

  def new
    @list_item = ListItem.new
    render json: @list_item, status: :ok, include: [:item, :list]
  end

  def create
    @list_item = ListItem.new(list_item_params)
    if @list_item.save
      render json: @list_item, status: :created, include: [:item, :list]
    else
      render json: @list_item.errors, status: 422
    end
  end

  def edit
    @list_item = ListItem.find(params[:id])
    if @list_item
      render json: @list_item, status: :ok, include: [:item, :list]
    else
      head 404
    end
  end

  def update
    @list_item = ListItem.find(params[:id])
    head 404 unless @list_item
    @list_item = ListItem.update(list_item_params)
    if @list_item.save
      render json: @list_item, status: :ok, include: [:item, :list]
    else
      render json: @list_item.errors, status: :ok
    end
  end

  def destroy
    @list_item = ListItem.find(params[:id])
    head 404 unless @list_item
    @list_item.destroy
    head :ok
  end

  private

  def list_item_params
    params.require(:list_item).permit(:item_id, :count, :is_bought, :is_cancel, :list_id)
  end
end
class Api::V1::ListsController < ApplicationController
  swagger_controller :lists, 'List Management'

  swagger_model :List do
    description 'A List object.'
    property :id, :integer, :required, 'List Id'
    property :shop_id, :integer, :required, 'shop Id'
    property :name, :string, :required, 'List name'
    property :alarm, :integer, :required, 'Alarm'
    property :owner_id, :integer, :required, 'owner id'
    property :group_id, :integer, :required, 'group id'
    property :created_at, :string, :optional, 'Created at'
    property :updated_at, :string, :optional, 'Update at'
  end

  swagger_api :index do
    summary 'Fetches all List objects'
    notes 'Dont work, try in Postman'
  end

  swagger_api :show do
    summary 'Fetches a single List object'
    param :path, :id, :integer, :optional, 'List Id'
    response :ok, 'Success', :List
    response :not_found
  end

  swagger_api :create do
    summary 'Creates a new List'
    param :form, 'list[name]', :string, :required, 'List Name'
    param :form, 'list[shop_id]', :integer, :required, 'shop id'
    param :form, 'list[group_id]', :integer, :required, 'group id'
    param :form, 'list[owner_id]', :integer, :required, 'owner id'
    param :form, 'list[alarm]', :integer, :required, 'alarm'
    response :created, 'created', :List
    response :not_acceptable
  end

  swagger_api :update do
    summary 'Updates an existing List'

    param :path, :id, :integer, :required, 'List Id'
    param :form, 'list[name]', :string, :required, 'List Name'
    param :form, 'list[shop_id]', :integer, :required, 'shop id'
    param :form, 'list[group_id]', :integer, :required, 'group id'
    param :form, 'list[owner_id]', :integer, :required, 'owner id'
    param :form, 'list[alarm]', :integer, :required, 'alarm'
    response :ok, 'ok', :List
    response :not_found
  end

  swagger_api :destroy do
    summary 'Deletes an existing List object'
    notes 'Status 200 dont work, try in Postman'
    param :path, :id, :integer, :required, 'List Id'
    response :ok, 'ok', :List
    response :not_found
  end

  def index
    @lists = List.all
    render json: @lists, status: :ok, include: [:shop, :group, :list_items, :users]
  end

  def show
    @list = List.find(params[:id])
    if @list
      render json: @list, status: :ok, include: [:shop, :group, :list_items, :users]
    else
      head 404
    end
  end

  def new
    @list = List.new
    render json: @list, status: :ok, include: [:shop, :group, :list_items, :users]
  end

  def create
    @list = List.new(list_params)
    if @list.save
      params[:group_id] ? @list.users << @list.group.users : @list.users << current_user

      render json: @list, status: :created, include: [:shop, :group, :list_items, :users]
    else
      render json: @list.errors, status: 422
    end
  end

  def edit
    @list = List.find(params[:id])
    if @list
      render json: @list, status: :ok, include: [:shop, :group, :list_items, :users]
    else
      head 404
    end
  end

  def update
    @list = List.find(params[:id])
    head 404 unless @list
    @list = List.update(list_params)
    if @list.save
      render json: @list, status: :ok, include: [:shop, :group, :list_items, :users]
    else
      render json: @list.errors, status: :ok
    end
  end

  # TODO:
  # api_v1_list DELETE /api/v1/lists/:id(.:format)      api/v1/lists#destroy <-- if @current.user == @list.users.first
  def destroy
    @list = List.find(params[:id])
    head 404 unless @list
    @list.destroy
    head :ok
  end

  private

  def list_params
    params.require(:list).permit(:name, :shop_id, :alarm, :group_id, :owner_id)
  end
end

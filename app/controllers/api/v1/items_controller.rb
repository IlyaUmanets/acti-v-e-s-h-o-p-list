class Api::V1::ItemsController < ApplicationController
  swagger_controller :items, 'Item Management'

  swagger_model :Item do
    description 'A Item object.'
    property :id, :integer, :required, 'Item Id'
    property :name, :string, :required, 'Name'
    property :description, :string, :optional, 'Description'
    property :category_id, :integer, :required, 'Category Id'
    property :created_at, :string, :optional, 'Created at'
    property :updated_at, :string, :optional, 'Update at'
  end

  swagger_api :index do
    summary 'Fetches all Item objects'
    notes 'Dont work, try in Postman'
  end

  swagger_api :show do
    summary 'Fetches a single Item object'
    param :path, :id, :integer, :optional, 'Item Id'
    response :ok, 'Success', :Item
    response :not_found
  end

  swagger_api :create do
    summary 'Creates a new Item'
    param :form, 'item[name]', :string, :required, 'Item name'
    param :form, 'item[category_id]', :string, :required, 'Item category_id'
    param :form, 'item[description]', :string, :required, 'Item description'
    response :created, 'created', :Item
    response :not_acceptable
  end

  swagger_api :update do
    summary 'Updates an existing item'

    param :path, :id, :integer, :required, 'Item Id'
    param :form, 'item[name]', :string, :required, 'Category name'
    param :form, 'item[category_id]', :string, :required, 'Item category_id'
    param :form, 'item[description]', :string, :required, 'Item description'
    response :ok, 'ok', :Item
    response :not_found
  end

  # dont show 200
  swagger_api :destroy do
    summary 'Deletes an existing Item object'
    notes 'Status 200 dont work, try in Postman'
    param :path, :id, :integer, :required, 'Item Id'
    response :ok, 'ok', :Item
    response :not_found
  end

  def index
    @items = Item.all
    render json: @items, status: :ok, include: [:category, :list_items]
  end

  def show
    @item = Item.find(params[:id])
    if @item
      render json: @item, status: :ok, include: [:category, :list_items]
    else
      head 404
    end
  end

  def new
    @item = Item.new
    render json: @item, status: :ok, include: [:category, :list_items]
  end

  def create
    @item = Item.new(item_params)
    if @item.save
      render json: @item, status: :created, include: [:category, :list_items]
    else
      render json: @item.errors, status: 422
    end
  end

  def edit
    @item = Item.find(params[:id])
    if @item
      render json: @item, status: :ok, include: [:category, :list_items]
    else
      head 404
    end
  end

  def update
    @item = Item.find(params[:id])
    head 404 unless @item
    @item = Item.update(item_params)
    if @item.save
      render json: @item, status: :ok, include: [:category, :list_items]
    else
      render json: @item.errors, status: :ok
    end
  end

  def destroy
    @item = Item.find(params[:id])
    head 404 unless @item
    @item.destroy
    head :ok
  end

  private

  def item_params
    params.require(:item).permit(:name, :description, :category_id)
  end
end

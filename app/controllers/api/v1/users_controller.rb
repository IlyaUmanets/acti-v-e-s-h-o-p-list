class Api::V1::UsersController < ApplicationController
  # FIXME: enable later
  # before_filter :restrict_access

  # param(param_type, name, type, required, description = nil, hash={})
  swagger_controller :users, 'User Management'

  swagger_model :User do
    description 'A User object.'
    property :id, :integer, :required, 'User Id'
    property :name, :string, :required, 'Name'
    property :auth_token, :string, :required, 'Auth token'
    property :provider, :string, :required, 'Social provider'
    property :uid, :string, :required, 'Social user id'
  end

  # FIXME: Dont show all users
  swagger_api :index do
    summary 'Fetches all Userender json: {records: @user, status: :ok}.to_json(:include => :socials)r items'
    notes 'Dont work, try in Postman'
  end

  swagger_api :show do
    summary 'Fetches a single User item'
    param :path, :id, :integer, :optional, 'User Id'
    response :ok, 'Success', :User
    response :not_found
  end

  swagger_api :create do
    summary 'Creates a new User'
    param :form, 'user[name]', :string, :required, 'Name'
    param :form, 'user[provider]', :string, :required, 'Social provider'
    param :form, 'user[uid]', :string, :required, 'Social user id'
    response :created, 'created', :User
    response :not_acceptable
  end

  swagger_api :update do
    summary 'Updates an existing User'

    param :path, :id, :integer, :required, 'User Id'
    param :form, 'user[name]', :string, :required, 'Token'
    response :ok, 'ok', :User
    response :not_found
  end

  # dont show 200
  swagger_api :destroy do
    summary 'Deletes an existing User item'
    notes 'Status 200 dont work, try in Postman'
    param :path, :id, :integer, :required, 'User Id'
    response :ok, 'ok', :User
    response :not_found
  end

  def index
    @users = User.all
    render json: @users, status: :ok, include: [:socials, :groups, :friends, :friendships, :lists, :shops]
  end

  def show
    @user = User.find(params[:id])
    if @user
      render :json => @user, status: :ok, include: [:socials, :groups, :friends, :friendships, :lists, :shops]
    else
      head 404
    end
  end

  # Remove?
  def new
    @user = User.new
    render json: @user, status: :ok, include: [:socials, :groups, :friends, :friendships, :lists, :shops]
  end

  def create
    binding.pry
    @user = User.new(name: user_params[:name], auth_token: @auth_token)
    @user.socials.new(uid: social_params[:uid], provider: social_params[:provider])
    if @user.save
      render json: @user, status: :created, include: [:socials, :groups, :friends, :friendships, :lists, :shops]
    else
      render json: @user.errors, status: 422
    end
  end

  # Remove?
  def edit
    @user = User.find(params[:id])
    if @user
      render json: @user, status: :ok, include: [:socials, :groups, :friends, :friendships, :lists, :shops]
    else
      head 404
    end
  end

  # check if head 400 is valid
  def update
    @user = User.find(params[:id])
    head 404 unless @user
    @user = @user.update(user_params)
    if @user.save
      render json: @user, status: :ok, include: [:socials, :groups, :friends, :friendships, :lists, :shops]
    else
      render json: @user.errors
      head 400
    end
  end

  def destroy
    @user = User.find(params[:id])
    if @user
      @user.destroy
      head 200
    else
      head 404
    end
  end

  private

  def social_params
    params.require(:user).permit(:uid, :provider)
  end

  def user_params
    params.require(:user).permit(:name, :auth_token)
  end

  # FIXME: rewrite api_key condition
  # FIXME: remove after swagger tested
  def restrict_access
    return true if request.content_type == 'application/x-www-form-urlencoded' || params[:api_key]
    # return true if params[:api_key]
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end
end

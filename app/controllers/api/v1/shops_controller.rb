class Api::V1::ShopsController < ApplicationController
  swagger_controller :shops, 'Shop Management'

  swagger_model :Shop do
    description 'A Shop object'
    property :id, :integer, :required, 'Shop Id'
    property :name, :string, :required, 'Shop name'
    property :longitude, :float, :required, 'longitude'
    property :latitude, :float, :required, 'latitude'
    property :is_popular, :boolean, :required, 'is popular'
    property :created_at, :string, :optional, 'Created at'
    property :updated_at, :string, :optional, 'Update at'
  end

  swagger_api :index do
    summary 'Fetches all Shop objects'
    notes 'Dont work, try in Postman'
  end

  swagger_api :show do
    summary 'Fetches a single Shop object'
    param :path, :id, :integer, :optional, 'Shop Id'
    response :ok, 'Success', :Shop
    response :not_found
  end

  swagger_api :create do
    summary 'Creates a new Shop'
    param :form, 'shop[name]', :string, :required, 'shop Name'
    param :form, 'shop[longtitude]', :float, :required, 'longtitude'
    param :form, 'shop[latitude]', :float, :required, 'latitude'
    param :form, 'shop[is_popular]', :boolean, :required, 'is popular'
    response :created, 'created', :Shop
    response :not_acceptable
  end

  swagger_api :update do
    summary 'Updates an existing List'

    param :path, :id, :integer, :required, 'List Id'
    param :form, 'shop[name]', :string, :required, 'shop Name'
    param :form, 'shop[longtitude]', :float, :required, 'longtitude'
    param :form, 'shop[latitude]', :float, :required, 'latitude'
    param :form, 'shop[is_popular]', :boolean, :required, 'is popular'
    response :ok, 'ok', :Shop
    response :not_found
  end

  swagger_api :destroy do
    summary 'Deletes an existing Shop object'
    notes 'Status 200 dont work, try in Postman'
    param :path, :id, :integer, :required, 'Shop Id'
    response :ok, 'ok', :Shop
    response :not_found
  end

  def index
    @shop = Shop.all
    render json: @shop, status: :ok, include: [:users, :lists]
  end

  def show
    @shop = Shop.find(params[:id])
    if @shop
      render json: @shop, status: :ok, include: [:users, :lists]
    else
      head 404
    end
  end

  def new
    @shop = Shop.new
    render json: @shop, status: :ok, include: [:users, :lists]
  end

  def create
    @shop = Shop.new(shop_params)
    if @shop.save
      render json: @shop, status: :created, include: [:users, :lists]
    else
      render json: @shop.errors, status: 422
    end
  end

  def edit
    @shop = Shop.find(params[:id])
    if @shop
      render json: @shop, status: :ok, include: [:users, :lists]
    else
      head 404
    end
  end

  def update
    @shop = Shop.find(params[:id])
    head 404 unless @shop
    @shop = Shop.update(shop_params)
    if @shop.save
      render json: @shop, status: :ok, include: [:users, :lists]
    else
      render json: @shop.errors, status: :ok
    end
  end

  def destroy
    @shop = Shop.find(params[:id])
    head 404 unless @shop
    @shop.destroy
    head :ok
  end

  private

  def shop_params
    params.require(:shop).permit(:name, :longtitude, :latitude, :is_popular)
  end
end

class Api::V1::FriendshipsController < ApplicationController
  swagger_controller :friendships, 'Friendship Management'

  swagger_model :Friendship do
    description 'A Friendship object.'
    property :id, :integer, :required, 'Friendship Id'
    property :user_id, :integer, :optional, 'User Id'
    property :friend_id, :integer, :optional, 'Friend Id'
    property :created_at, :string, :optional, 'Created at'
    property :updated_at, :string, :optional, 'Update at'
  end
  # FIXME: show wrong method
  swagger_api :create do
    summary 'Creates a new Friendship'
    param :form, 'friendship[user_id]', :string, :required, 'User id'
    param :form, 'friendship[friend_id]', :string, :required, 'Friend id'
    response :created, 'created', :Friendship
    response :not_acceptable
  end

  # dont show params id
  swagger_api :destroy do
    summary 'Deletes an existing friendship item'
    notes 'Status 200 dont work, try in Postman'
    param :path, :id, :integer, :required, 'Friendship Id'
    response :ok, 'ok', :Friendship
    response :not_found
  end

  def create
    @friendship = current_user.friendships.build(friend_id: params[:friend_id])
    if @friendship.save
      render json: @friendship, status: 201, include: [:user, :friend]
    else
      render json: @friendship.errors, status: 422
    end
  end

  def destroy
    @friendship = current_user.friendships.find(params[:id])
    head 404 unless @friendship
    @friendship.destroy
    head :ok
  end
end

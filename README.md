# README
> List of [request params](#params) and [actions](#actions), wich allowed for current API. <br>
> [Headers](#headers) is common for all API requests.

### Headers
 - **Accept:** Application/json
 - **Content-Type:** Application/json
 - **Authorization:** Token token="8347b2c85aabb16dc1d7f95cacfc62e5"


<a name="actions"/>
## List of request params:
___ 
| Action        | Request params | Method  | Type  | URL  | Response code |  Response body |
| :-----------: |:-------------------:| :---: | :---: | :---------: | :---------: |
| create user(example)   | [link](#user_create) | POST | JSON | localhost:3000/api/v1/users | 200 - cool; 404 - not cool| example |
| get list of lists | ---- | POST | JSON | localhost:3000/api/v1/lists | 200 - OK | [200](#list_get_200) |
| create list | [link](#list_create) | POST | JSON | localhost:3000/api/v1/lists | 201 - created | [201](#list_create_201) |
| leave this example <br> for next table updates | example | example | example | example | example | example |

<br>
<a name="params"/>
## List of request params:
___
<a name="user_create"/>
### User create
```json
{
   "user":{
      "email":"email@example.com",
      "password":"example_passrod",
      "password_confirmation":"example_passrod",
      "phone":"+380931234567",
      "full_name":"Sarah Connor",
      "is_conditions_confirmed":"true"
   }
}
```

### List_create
```json
{
   "list":{
      "name":"test_list"
   }
}
```
<br>
## List of request params:
___

<a name="list_get_200"/>
### Lists get: 200
```json
[
    {
        "id": 1,
        "shop_id": null,
        "name": null,
        "alarm": null,
        "created_at": "2015-03-02T00:17:53.915Z",
        "updated_at": "2015-03-02T00:17:53.915Z"
    },
    {
        "id": 2,
        "shop_id": null,
        "name": null,
        "alarm": null,
        "created_at": "2015-03-02T00:18:53.058Z",
        "updated_at": "2015-03-02T00:18:53.058Z"
    },
    {
        "id": 3,
        "shop_id": null,
        "name": "test_list",
        "alarm": null,
        "created_at": "2015-03-02T01:10:45.067Z",
        "updated_at": "2015-03-02T01:10:45.067Z"
    },
    {
        "id": 4,
        "shop_id": null,
        "name": "test_list",
        "alarm": null,
        "created_at": "2015-03-02T01:13:45.537Z",
        "updated_at": "2015-03-02T01:13:45.537Z"
    }
]
```
<a name="list_create_201"/>
### List create: 201
```json
{
    "id": 4,
    "shop_id": null,
    "name": "test_list",
    "alarm": null,
    "created_at": "2015-03-02T01:13:45.537Z",
    "updated_at": "2015-03-02T01:13:45.537Z"
}
```
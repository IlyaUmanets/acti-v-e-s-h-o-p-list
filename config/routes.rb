Rails.application.routes.draw do
  mount GrapeSwaggerRails::Engine => '/'

  namespace :api do
    namespace :v1 do
      get 'friendships/create'
    end
  end

  namespace :api do
  namespace :v1 do
    get 'friendships/destroy'
    end
  end

  namespace :api do
    namespace :v1 do
      resource :sessions, only: [:create, :destroy]
      resources :users do
        get 'add_social', to: 'socials#add_social'
        resources :socials
      end
      resource :friendships, only: [:create, :destroy]
      resources :socials
      resources :categories
      resources :items
      resources :list_items
      resources :shops
      resources :lists
    end
  end
end
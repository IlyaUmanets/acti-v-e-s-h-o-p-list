source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc
# PostgreSQL
gem 'pg', group: :production
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
gem 'unicorn'

# Use Capistrano for deployment
gem 'capistrano', '~> 3.3.5'
gem 'capistrano-rails', '~> 1.1.2'

gem 'cancancan'

# Swagger docs generator
gem 'swagger-docs'
# Swagger UI
gem 'grape-swagger-rails'

group :development, :test do
  # Advanced rails console
  gem 'pry'
  # Advanced console output
  gem 'awesome_print'
  # Annotating for models
  gem 'annotate'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  # Use sqlite3 as the database for Active Record
  gem 'sqlite3'

  gem 'rspec-rails'

  gem 'factory_girl_rails'

  gem 'database_cleaner'

  gem 'simplecov'
end

group :assets do
  gem 'uglifier'
end

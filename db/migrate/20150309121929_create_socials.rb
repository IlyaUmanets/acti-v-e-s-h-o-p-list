class CreateSocials < ActiveRecord::Migration
  def change
    create_table :socials do |t|
      t.references :user, index: true
      t.integer :uid
      t.string :provider

      t.timestamps null: false
    end
    add_foreign_key :socials, :users
  end
end

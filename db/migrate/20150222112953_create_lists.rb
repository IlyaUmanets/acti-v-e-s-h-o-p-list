class CreateLists < ActiveRecord::Migration
  def change
    create_table :lists do |t|
      t.references :shop, index: true
      t.string :name
      t.integer :alarm

      t.timestamps null: false
    end
    add_foreign_key :lists, :shops
  end
end

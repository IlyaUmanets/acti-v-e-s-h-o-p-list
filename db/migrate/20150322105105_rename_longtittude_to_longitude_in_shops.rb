class RenameLongtittudeToLongitudeInShops < ActiveRecord::Migration
  def change
    rename_column :shops, :longtitude, :longitude
  end
end

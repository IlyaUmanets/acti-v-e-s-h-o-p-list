class CreateListItems < ActiveRecord::Migration
  def change
    create_table :list_items do |t|
      t.references :item, index: true
      t
      t.float :count
      t.boolean :is_bought
      t.boolean :is_cancel

      t.timestamps null: false
    end
    add_foreign_key :list_items, :items
  end
end

class AddGroupsToLists < ActiveRecord::Migration
  def change
    add_reference :lists, :group, index: true
  end
end

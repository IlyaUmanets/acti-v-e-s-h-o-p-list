class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string :name
      t.float :longtitude
      t.float :latitude
      t.boolean :is_popular

      t.timestamps null: false
    end
  end
end

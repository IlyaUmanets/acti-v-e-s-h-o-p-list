class UserSchemaFixes < ActiveRecord::Migration
  def change
    add_column :users, :access_token, :string
    remove_column :users, :password_salt, :string
    remove_column :users, :password_hash, :string
    remove_column :users, :email, :string
  end
end

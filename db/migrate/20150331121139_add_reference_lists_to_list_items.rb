class AddReferenceListsToListItems < ActiveRecord::Migration
  def change
    add_reference :list_items, :list, index: true
  end
end

class RenameUserAccesTokenToAuthToken < ActiveRecord::Migration
  def change
    rename_column(:users, :access_token, :auth_token)
  end
end
# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150401060709) do

  create_table "api_keys", force: :cascade do |t|
    t.string   "access_token"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "parent_id"
  end

  create_table "friendships", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "friend_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "groups", force: :cascade do |t|
    t.integer  "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name"
  end

  add_index "groups", ["owner_id"], name: "index_groups_on_owner_id"

  create_table "groups_users", id: false, force: :cascade do |t|
    t.integer "group_id"
    t.integer "user_id"
  end

  add_index "groups_users", ["group_id"], name: "index_groups_users_on_group_id"
  add_index "groups_users", ["user_id"], name: "index_groups_users_on_user_id"

  create_table "items", force: :cascade do |t|
    t.string   "name"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "description"
  end

  add_index "items", ["category_id"], name: "index_items_on_category_id"

  create_table "list_items", force: :cascade do |t|
    t.integer  "item_id"
    t.float    "count"
    t.boolean  "is_bought"
    t.boolean  "is_cancel"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "list_id"
  end

  add_index "list_items", ["item_id"], name: "index_list_items_on_item_id"
  add_index "list_items", ["list_id"], name: "index_list_items_on_list_id"

  create_table "lists", force: :cascade do |t|
    t.integer  "shop_id"
    t.string   "name"
    t.integer  "alarm"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "group_id"
    t.integer  "owner_id"
  end

  add_index "lists", ["group_id"], name: "index_lists_on_group_id"
  add_index "lists", ["shop_id"], name: "index_lists_on_shop_id"

  create_table "lists_users", id: false, force: :cascade do |t|
    t.integer "list_id"
    t.integer "user_id"
  end

  add_index "lists_users", ["list_id"], name: "index_lists_users_on_list_id"
  add_index "lists_users", ["user_id"], name: "index_lists_users_on_user_id"

  create_table "shops", force: :cascade do |t|
    t.string   "name"
    t.float    "longitude"
    t.float    "latitude"
    t.boolean  "is_popular"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shops_users", id: false, force: :cascade do |t|
    t.integer "shop_id"
    t.integer "user_id"
  end

  add_index "shops_users", ["shop_id"], name: "index_shops_users_on_shop_id"
  add_index "shops_users", ["user_id"], name: "index_shops_users_on_user_id"

  create_table "socials", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "uid"
    t.string   "provider"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "socials", ["user_id"], name: "index_socials_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "auth_token"
    t.boolean  "is_admin",   default: false
  end

end
